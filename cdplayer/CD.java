package at.mager.cdplayer;

import java.util.ArrayList;
import java.util.List;

public class CD {
	private String name;
	private List<Song> songs;

	public CD(String name) {
		super();
		this.name = name;
		this.songs=new ArrayList<Song>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

	public void addSong(Song songs) {
		this.songs.add(songs);
	}
}