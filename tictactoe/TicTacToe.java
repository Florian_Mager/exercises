package at.mager.tictactoe;

import java.util.Scanner;

public class TicTacToe {
	char[][] board = new char[3][3];
	Scanner s = new Scanner(System.in);
	char player = 'x';
	boolean run = true;
	boolean dontChange = false;
	
	public boolean checkWin() {
		boolean winner = false;
		for (int row = 0; row <= 2; row++) {
			if (board[row][0] == board[row][1] && board[row][1] == board[row][2] && board[row][0] > 0
					|| board[0][row] == board[1][row] && board[1][row] == board[2][row] && board[0][row] > 0) {
				winner = true;
			}
		}
		if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] > 0) {
			winner = true;
		}
		if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] > 0) {
			winner = true;
		}
		return winner;
	}
	
	public void player() {
		if (player == 'x') {
			player = 'o';
		} else if (player == 'o') {
			player = 'x';
		}
	}
	
	public boolean addInput(String[] splitInput) {
		int row = Integer.parseInt(splitInput[0]);
		int col = Integer.parseInt(splitInput[1]);
		
		if (board[row][col] == 'x' || board[row][col] == 'o') {
			dontChange = true;
			System.out.println("Fehler. Das Feld ist bereits befüllt.");
		} else {
			dontChange = false;
			board[row][col] = player;
		}
		return dontChange;
	}
	
	public void printBoard() {
		
		System.out.println(" ___________");
		System.out.println("| " + board[0][0] + " | " + board[0][1] + " | " + board[0][2] + " |");
		System.out.println("|-----------|");
		System.out.println("| " + board[1][0] + " | " + board[1][1] + " | " + board[1][2] + " |");
		System.out.println("|-----------|");
		System.out.println("| " + board[2][0] + " | " + board[2][1] + " | " + board[2][2] + " |");
		System.out.println("|___________|");
		System.out.println("");
		if(run) {
			System.out.println("In welches Feld wollen Sie etwas schreiben?");
		}
	}
	
	public String[] getInput() {
		String input = s.next();
		String[] split = input.split(",");
		return split;
	}
	
	public void startTicTacToe() {
		while (run) {
			printBoard();
			String[] splitInput = getInput();
			addInput(splitInput);
			boolean isThereAWinner = checkWin();
			if (isThereAWinner) {
				run = false;
				printBoard();
				System.out.println("Yeah! Spieler " + player + " hat die Runde gewonnen!");
			}
			if (!dontChange) {
				player();		
			}
		}
	}

}