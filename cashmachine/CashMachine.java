package at.mager.cashmachine;

import java.util.Scanner;

public class CashMachine {
	private int balance = 5000;
	private int withdraw;
	private int deposit;
		
    Scanner s = new Scanner(System.in);
    	
    public void startCashMachine(){
    	while(true) {
            System.out.println("CashMachine");
            System.out.println("  1> Abheben");
            System.out.println("  2> Einzahlen");
            System.out.println("  3> Kontostand");
            System.out.println("  4> Verlassen");
            System.out.println("");
            System.out.print("  Was m�chtest du tun? Gib die entsprechende Zahl ein: ");
            int n = s.nextInt();
            switch(n) {
               	case 1:
               		System.out.println("");
                	System.out.print("  Gib einen Betrag ein: ");
                	withdraw = s.nextInt();
                if (balance >= withdraw) {
                	balance = balance - withdraw;
                	System.out.println("");
                	System.out.println("  Erfolgreich! Entnehmen Sie ihr Geld.");
                } else {
                	System.out.println("  Du hast nicht genug Geld.");
                }
                	System.out.println("");
                break;
                	
                case 2:
                	System.out.println("");
                	System.out.print("  Einzahlungsbetrag: ");
                	deposit = s.nextInt();
                	balance = balance + deposit;
                	System.out.println("");
                	System.out.println("  Dein Geld wurde erfolgreich eingezahlt.");
                	System.out.println("");
                break;
                
                case 3:
                	System.out.println("");
                	System.out.println("  Geld: " + balance);
                	System.out.println("");
                break;

                case 4:
                	System.out.println("");
                	System.out.println("  Auf Wiedersehen!");
                	System.exit(0);
            }
        }
    }	
}