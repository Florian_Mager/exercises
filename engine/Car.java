package at.mager.engine;

public class Car{
	private Engine drive;
	
	public Car(Engine drive) {
		super();
		this.drive = drive;
	}
	
	public void gas(int amount) {
		int newAmount = amount / 2;
		this.drive.run(newAmount);
	}
	
	public void getSerial() {
		System.out.println("The serial number of your engine is: " + this.drive.getSerial());
	}
	
}