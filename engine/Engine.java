package at.mager.engine;

public interface Engine {
	public void run(int amount);
	
	public int getSerial();
	
}