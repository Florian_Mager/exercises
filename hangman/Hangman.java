package at.mager.hangman;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Hangman {
	String[] words = {"Notebook", "Coronavirus", "Schultasche", "Baumhaus", "Bildschirm", "Klopapier", "Messer", "Steckdose", "Diplomarbeit", "Rucksack"};
	char[] randomWord = new char[20];
	char[] hiddenWord = new char[20];
	Random r = new Random();
	Scanner s = new Scanner(System.in);
	int triesLeft;

	public boolean startHangman() {
		boolean win = false;
		int randomIndex;
		boolean endGame = false;
		char inputLetter;
		
		// s.useDelimiter("");
		
		while (!endGame) {
			System.out.println("HANGMAN");
			System.out.println("");
			System.out.println("Ein neues Spiel beginnt!");
			System.out.println("Sie haben 10 Versuche um das Wort zu erraten.");
			System.out.println("");
			triesLeft = 10;
			win = false;
			randomIndex = r.nextInt(10);
			randomWord = words[randomIndex].toCharArray();
			hiddenWord = words[randomIndex].toCharArray();
			Arrays.fill(hiddenWord, 0, randomWord.length, '*');
		
			while ((!win) && (triesLeft > 0)) {
				output();
			
				// Buchstabe abfragen
				inputLetter = getInput();
			
				// Pr�fen, ob Buchstabe in L�sungswort vorhanden
				if (checkInput(inputLetter)) {
					if (Arrays.equals(randomWord, hiddenWord)) {
						win = true;
					}
				}
			}
			// Pr�fen, ob R�tsel gel�st
			if (win) {
				endGame = !playAgain();
			} else if (triesLeft == 0) {
				System.out.println("");
				System.out.println("Verloren! Alle Versuche aufgebraucht!");
				System.out.println("Das L�sungswort war " + String.valueOf(randomWord) + ".");
				endGame = !playAgain();
			}
		
		}
		
		return win;
	}
	
	public boolean playAgain() {
		char inputLetter = '*';
		boolean again = true;
		
		System.out.println("");
		System.out.println("Sie haben gewonnen!");
		System.out.println("Das L�sungswort war " + String.valueOf(randomWord) + ".");
		System.out.println("Wollen Sie nochmal spielen? j / n");
		while ((Character.toUpperCase(inputLetter) != 'N') && (Character.toUpperCase(inputLetter) != 'J')) {
			inputLetter = getInput();
		}
		if (Character.toUpperCase(inputLetter) == 'N') {
			again = false;
			System.out.println("Auf Wiedersehen!");
			System.exit(0);
		}
		
		return again;
	}
	
	public void output() {
		System.out.print(hiddenWord);
		System.out.println("               Versuche �brig: " + triesLeft);
	}
	
	public boolean checkInput(char inputLetter) {
		boolean found = false;
		
		for (int j = 0; j < randomWord.length; j++) {
			if (((randomWord[j] == Character.toLowerCase(inputLetter)) || (randomWord[j] == Character.toUpperCase(inputLetter)))) {
				found = true;
				hiddenWord[j] = randomWord[j];
			}
		}
		if (!found) {
			triesLeft--;
		}
		return found;
	}
	
	public char getInput() {
		char input = s.next().charAt(0);
		return input;
	}
	
}