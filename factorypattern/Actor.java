package at.mager.factorypattern;

public interface Actor {
	public void move();
	
	public void render();
}