package at.mager.factorypattern;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Game g = new Game();
		Snowflake s1 = new Snowflake();
		Homer h1 = new Homer();
		RandomActor ra = new RandomActor();
		
		ra.createActor();
		g.addActor(ra.createActor());
		g.renderAll();
	}

}