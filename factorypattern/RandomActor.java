package at.mager.factorypattern;

public class RandomActor {	
	
	public Actor createActor() {
		int random;
		Actor newActor = null;
		random = (int) (Math.random() * ((3 - 1) + 1)) + 1;
		
		if (random == 1) {
			newActor = new Homer();
		} else if (random == 2) {
			newActor = new Snowflake();
		}
		return newActor;
	}
}