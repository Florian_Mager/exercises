package at.mager.footballxml;

import java.util.Date;

public class Player implements NewsletterReceiver {
	private String name;
	private Date birthday;
	private int value;
	
	public Player(String name, Date birthday, int value) {
		super();
		this.name = name;
		this.birthday = birthday;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public void sendInfo(String msg) {
		// TODO Auto-generated method stub
		System.out.println(msg);
	}
	
}