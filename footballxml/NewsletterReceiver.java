package at.mager.footballxml;

public interface NewsletterReceiver {
	public void sendInfo(String msg);
}