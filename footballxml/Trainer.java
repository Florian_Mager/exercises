package at.mager.footballxml;

public class Trainer implements NewsletterReceiver {
	private String name;

	public Trainer(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void sendInfo(String msg) {
		// TODO Auto-generated method stub
		System.out.println(msg);
	}
	
}