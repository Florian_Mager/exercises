package at.mager.footballxml;

import java.util.ArrayList;
import java.util.List;

public class NewsletterManager {
	private List<NewsletterReceiver> receiverList;

	public NewsletterManager() {
	super();
		this.receiverList = new ArrayList<NewsletterReceiver>();
	}
	  
	public void addReceiver(Player playerName ) {
		receiverList.add(playerName);
	}
	
	public void addReceiver(Trainer trainerName ) {
		receiverList.add(trainerName);
	}
	  
	public void sendInfo(String msg) {
		for (NewsletterReceiver newsletterReceiver : receiverList) {
			newsletterReceiver.sendInfo(msg);
	    }
	}
}