package at.mager.footballxml;

import java.util.Date;

public class Goalkeeper extends Player {
	private int reactionTime; 
	
	public Goalkeeper(String name, Date birthday, int value, int reactionTime) {
		super(name, birthday, value);
		this.reactionTime = reactionTime;
	}

	public int getReactionTime() {
		return reactionTime;
	}

	public void setReactionTime(int reactionTime) {
		this.reactionTime = reactionTime;
	}	
		
}