package at.mager.footballxml;

import java.util.Date;

public class Main {

	public static void main(String[] args) {
		Fieldplayer fp1 = new Fieldplayer("Messi", new Date(), 3000000);
		Fieldplayer fp2 = new Fieldplayer("Ronaldo", new Date(), 2000000);
		Goalkeeper gk1 = new Goalkeeper("Neuer", new Date(), 1000000, 50);
		
		Team t1 = new Team("5cWI");
		t1.addPlayer(fp1);
		t1.addPlayer(fp2);
		
		System.out.println("Team:  " + t1.getName());
		System.out.println("Value: " + t1.getTeamValue() + "$");
		
		NewsletterManager nlm = new NewsletterManager();
	    
	    nlm.addReceiver(fp1);
	    nlm.addReceiver(gk1);
	    
	    nlm.sendInfo("Newsletter received!");
	}

}