package at.mager.encryption;

public interface Encryptor {
	public String encrypt(String message);
	public String decrypt(String message);
}