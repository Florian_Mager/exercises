package at.mager.encryption;

public class CaesarEncryption implements Encryptor {
	public int shift;

	public CaesarEncryption() {
		super();
		this.shift = 2;
	}

	@Override
	public String encrypt(String message) {
		String encrypted = "";
		
		for (int i = 0; i < message.length(); i++) {
			char ch = message.charAt(i);
			int ac = (int) ch;
			char shifted = (char) (ac + shift);
			if (ac <= 122 && ac >= 97) {
				if (shifted > 122) {
					shifted -= 26;
				}
				encrypted += shifted; 
			}
			else if (ac <= 90 && ac >= 65) {
				if (shifted > 90) {
					shifted -= 26;
				}
				encrypted += shifted; 
			} else {
				encrypted += ch;
			}
		
		}
		return encrypted;
	}
	
	@Override
	public String decrypt(String message) {
		String encrypted = "";

		for (int i = 0; i < message.length(); i++) {
			char ch = message.charAt(i);
			int ac = (int) ch;
			char shifted = (char) (ac - shift);
			if (ac <= 122 && ac >= 97) {
				if (shifted < 97) {
					shifted += 26;
				}
				encrypted += shifted; 
			}
			else if (ac <= 90 && ac >= 65) {
				if (shifted > 65) {
					shifted += 26;
				}
				encrypted += shifted; 
			} else {
				encrypted += ch;
			}
		}
		return encrypted;
	} 
}