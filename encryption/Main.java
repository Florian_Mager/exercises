package at.mager.encryption;

public class Main {

	public static void main(String[] args) {
		Encryptor e = new CaesarEncryption();
		
		// encrypt
		System.out.println(e.encrypt("hello y"));
		
		// decrypt
		System.out.println(e.decrypt("jgnnq a"));

	}

}