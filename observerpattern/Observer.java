package at.mager.observerpattern;

public interface Observer {
	public void informAll();
	
	public void addObservable(Observable observable);
}