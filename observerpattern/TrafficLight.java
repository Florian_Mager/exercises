package at.mager.observerpattern;

public class TrafficLight implements Observable{
	private int id;
	
	public TrafficLight(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public void inform() {
		// TODO Auto-generated method stub
		System.out.println("I am the TrafficLight with number " + id + ".");
	}

}