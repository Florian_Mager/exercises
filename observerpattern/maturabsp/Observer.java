package at.mager.observerpattern.maturabsp;

public interface Observer {
	public void informAll();
	
	public void addObservable(Observable observable);
}