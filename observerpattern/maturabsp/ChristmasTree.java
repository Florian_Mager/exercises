package at.mager.observerpattern.maturabsp;

public class ChristmasTree implements Observable{
	private int id;
	
	public ChristmasTree(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public void inform() {
		// TODO Auto-generated method stub
		System.out.println("I am the ChristmasTree with number " + id + ".");
	}

}