package at.mager.observerpattern.maturabsp;

public interface Observable {
	public void inform();
}