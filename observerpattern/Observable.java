package at.mager.observerpattern;

public interface Observable {
	public void inform();
}