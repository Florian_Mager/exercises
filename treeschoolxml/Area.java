package at.mager.treeschoolxml;

import java.util.ArrayList;
import java.util.List;

public class Area {
	private String areaName;
	private int areaSize;
	private List<Tree> treeList;
	
	public Area(String name, int areaSize, List<Tree> treeList) {
		super();
		this.areaName = name;
		this.areaSize = areaSize;
		this.treeList = new ArrayList<Tree>();
	}

	public void doFertilize() {
	    for (Tree tree : treeList) {
	      tree.getStrategy().doFertilize(areaName);
	    }
	  }
	
	public void addTree(Tree tree) {
		treeList.add(tree);
	}
	
	public String getName() {
		return areaName;
	}

	public void setName(String name) {
		this.areaName = name;
	}

	public int getAreaSize() {
		return areaSize;
	}

	public void setAreaSize(int areaSize) {
		this.areaSize = areaSize;
	}

	public List<Tree> getTreeList() {
		return treeList;
	}

	public void setTreeList(List<Tree> treeList) {
		this.treeList = treeList;
	}
	
}