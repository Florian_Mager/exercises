package at.mager.treeschoolxml;

public class Tree implements FertilizeStrategy {
	private int id;
	private FertilizeStrategy strategy;
	
	public Tree(int id, FertilizeStrategy strategy) {
		super();
		this.id = id;
		this.strategy = strategy;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FertilizeStrategy getStrategy() {
		return strategy;
	}

	public void setStrategy(FertilizeStrategy strategy) {
		this.strategy = strategy;
	}

	@Override
	public void doFertilize(String strategy) {
		// TODO Auto-generated method stub
		
	}
	
}