package at.mager.treeschoolxml;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Tree t1 = new LeafTree(1, new TopGreen());
		Tree t2 = new Conifer(2, new SuperGrow());
		
		Area a1 = new Area("myArea", 40, null);
		a1.addTree(t1);
		a1.addTree(t2);
		
		a1.doFertilize();
		
	}
	
}