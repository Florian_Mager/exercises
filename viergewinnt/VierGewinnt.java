package at.mager.viergewinnt;

import java.util.Scanner;

public class VierGewinnt {
	static final int ROW_COUNT = 6;
	static final int COL_COUNT = 7;
	char[][] board = new char[ROW_COUNT][COL_COUNT];
	Scanner s = new Scanner(System.in);
	char player = 'x';
	boolean run = true;
	boolean dontChange = false;
	
	public void startVierGewinnt() {
		initBoard();
		while (run) {
			printBoard();
			String doneInput = getInput();
			addInput(doneInput);
			boolean isThereAWinner = checkWin();
			if (isThereAWinner) {
				run = false;
				printBoard();
				System.out.println("Super! Spieler '" + player + "' hat die Runde gewonnen!");
			}
			if (!dontChange) {
				player();
			}
		}
	}
	
	public void initBoard() {
		for (int row = 0; row < ROW_COUNT; row++) {
			for (int col = 0; col < COL_COUNT; col++) {
				board[row][col] = ' ';
			}
		}
	}
	
	public char upperCaseWinner() {
		if (player == 'o') {
			return 'O';
		} else {
			return 'X';
		}
	}
	
	public boolean checkWin() {
		boolean winner = false;
		// Spalte
		for (int row = 0; (!winner) && (row <= ROW_COUNT - 1); row++) {
			for (int col  = 0; (!winner) && (col < COL_COUNT - 3); col++) {
					
				if (board[row][col] == player && board[row][col+1] == player && board[row][col+2] == player && board[row][col+3] == player) {
					winner = true;
					board[row][col] = upperCaseWinner();
					board[row][col+1] = upperCaseWinner();
					board[row][col+2] = upperCaseWinner();
					board[row][col+3] = upperCaseWinner();
				}
			}
		}
		// Reihe
		for (int col = 0; (!winner) && (col <= COL_COUNT - 1); col++) {

			for (int row  = 0; (!winner) && (row < ROW_COUNT - 3); row++) {
					
				if (board[row][col] == player && board[row+1][col] == player && board[row+2][col] == player && board[row+3][col] == player) {
					winner = true;
					board[row][col] = upperCaseWinner();
					board[row+1][col] = upperCaseWinner();
					board[row+2][col] = upperCaseWinner();
					board[row+3][col] = upperCaseWinner();
				}
			}
		}
		// diagonal
		for (int col = 0; (!winner) && (col < COL_COUNT - 3); col++) {
			for (int row  = ROW_COUNT - 1; row > ROW_COUNT - 3; row--) {
						
				if (board[row][col] == player && board[row-1][col+1] == player && board[row-2][col+2] == player && board[row-3][col+3] == player) {
					winner = true;
					board[row][col] = upperCaseWinner();
					board[row-1][col+1] = upperCaseWinner();
					board[row-2][col+2] = upperCaseWinner();
					board[row-3][col+3] = upperCaseWinner();
				}
			}
		}
				
		for (int col = COL_COUNT - 1; (!winner) && (col >= 3); col--) {
			for (int row  = ROW_COUNT - 1; row > ROW_COUNT - 3; row--) {
				if (board[row][col] == player && board[row-1][col-1] == player && board[row-2][col-2] == player && board[row-3][col-3] == player) {
					winner = true;
					board[row][col] = upperCaseWinner();
					board[row-1][col-1] = upperCaseWinner();
					board[row-2][col-2] = upperCaseWinner();
					board[row-3][col-3] = upperCaseWinner();
				}
			}
		}
		return winner;
	}
	
	public void player() {
		if (player == 'x') {
			player = 'o';
		} else {
			player = 'x';
		}
	}
	
	public void addInput(String doneInput) {
		try {
			int row = -1;
			int col = Integer.parseInt(doneInput) - 1;
		
			dontChange = false;
			while ((row < (ROW_COUNT - 1)) && (board[row+1][col] == ' ')) {
			row++;
			}
		
			if (row == -1) {
				dontChange = true;
				System.out.println("Fehler von Spieler '" + player + "'. Diese Spalte ist bereits voll gef�llt!");
			} else {
				board[row][col] = player;
			}
		
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(" Diese Spalte ist nicht vorhanden. W�hle eine Spalte von 1 bis " + COL_COUNT + ".");
		} catch (NumberFormatException e) {
			System.out.println(" Du darfst nur Nummern eingeben.");
		}
	}
	
	public void printBoard() {
		
		System.out.print("        ");
		for (int col = 1; col < COL_COUNT; col++) {
			System.out.print("____");
		}
		System.out.print("____");
		System.out.println("");
		System.out.print("       /");
		for (int col = 1; col < COL_COUNT; col++) {
			System.out.print("____");
		}
		System.out.println("___/|");
		System.out.print("       ");
		for (int col = 0; col < COL_COUNT; col++) {
			System.out.print("|   ");
		}
		System.out.println("||");
		for (int row = 0; row < ROW_COUNT; row++) {
			System.out.print("       |");
			for (int col = 0; col < COL_COUNT; col++) {
				
				System.out.print(" " + board[row][col] + " |");
			}
			System.out.println("|");
			if (row < (ROW_COUNT - 1)) {
				System.out.print("       |");
				for (int col = 1; col < COL_COUNT; col++) {
					System.out.print("---|");
				}
				System.out.println("---||");
			}
		}
		System.out.print("       |");
		for (int col = 1; col < COL_COUNT; col++) {
			System.out.print("___|");
		}
		System.out.println("___|/");
		System.out.println("");
		if(run) {
			System.out.println("Spieler '" + player + "' ist an der Reihe. In welches Feld willst du etwas schreiben?");
		}
	}
	
	public String getInput() {
		String input = s.next();
		return input;
	}

}